import * as React from 'react';
import { SVGProps } from 'react';

const SvgColumnChartPreview = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width={300}
    height={188}
    xmlns="http://www.w3.org/2000/svg"
    className="column-chart-preview_svg__apexcharts-svg"
    style={{
      background: '0 0',
    }}
    {...props}
  >
    <g
      className="column-chart-preview_svg__apexcharts-inner column-chart-preview_svg__apexcharts-graphical"
      transform="translate(22 30)"
    >
      <defs>
        <linearGradient
          id="column-chart-preview_svg__a"
          x1={0}
          y1={0}
          x2={0}
          y2={1}
        >
          <stop
            stopOpacity={0.4}
            stopColor="rgba(216,227,240,0.4)"
            offset={0}
          />
          <stop
            stopOpacity={0.5}
            stopColor="rgba(190,209,230,0.5)"
            offset={1}
          />
          <stop
            stopOpacity={0.5}
            stopColor="rgba(190,209,230,0.5)"
            offset={1}
          />
        </linearGradient>
        <clipPath id="column-chart-preview_svg__b">
          <rect
            width={274}
            height={145}
            x={-3}
            y={-1}
            rx={0}
            ry={0}
            strokeWidth={0}
            strokeDasharray={0}
            fill="#fff"
          />
        </clipPath>
      </defs>
      <rect
        width={18.76}
        height={143}
        rx={0}
        ry={0}
        fill="url(#column-chart-preview_svg__a)"
        className="column-chart-preview_svg__apexcharts-xcrosshairs"
        filter="none"
        fillOpacity={0.9}
      />
      <g
        className="column-chart-preview_svg__apexcharts-grid"
        strokeDasharray={0}
      >
        <g
          className="column-chart-preview_svg__apexcharts-gridlines-horizontal"
          stroke="#f1f1f1"
        >
          <path
            className="column-chart-preview_svg__apexcharts-gridline"
            d="M0 0h268M0 28.6h268M0 57.2h268M0 85.8h268M0 114.4h268M0 143h268"
          />
        </g>
        <path stroke="transparent" d="M0 143h268M0 1v142" />
      </g>
      <g
        className="column-chart-preview_svg__apexcharts-bar-series column-chart-preview_svg__apexcharts-plot-series"
        strokeLinecap="round"
        strokeWidth={2}
        strokeDasharray={0}
      >
        <g
          className="column-chart-preview_svg__apexcharts-series"
          fill="rgba(0,143,251,0.85)"
          stroke="#008ffb"
        >
          <path
            d="M4.02 143v-11.44q0 0 0 0h16.76q0 0 0 0h0V143h0zm26.8 0v-8.58q0 0 0 0h16.76q0 0 0 0h0V143h0zm26.8 0v-28.6q0 0 0 0h16.76q0 0 0 0h0V143h0zm26.8 0v-25.74q0 0 0 0h16.76q0 0 0 0h0V143h0zm26.8 0V60.06q0 0 0 0h16.76q0 0 0 0h0V143h0zm26.8 0V88.66q0 0 0 0h16.76q0 0 0 0h0V143h0zm26.8 0V71.5q0 0 0 0h16.76q0 0 0 0h0V143h0zm26.8 0v-25.74q0 0 0 0h16.76q0 0 0 0h0V143h0zm26.8 0v-34.32q0 0 0 0h16.76q0 0 0 0h0V143h0zm26.8 0v-20.02q0 0 0 0h16.76q0 0 0 0h0V143h0z"
            className="column-chart-preview_svg__apexcharts-bar-area"
            clipPath="url(#column-chart-preview_svg__b)"
          />
        </g>
        <g
          className="column-chart-preview_svg__apexcharts-series"
          fill="rgba(0,227,150,0.85)"
          stroke="#00e396"
        >
          <path
            d="M4.02 131.56v-5.72q0 0 0 0h16.76q0 0 0 0h0v5.72h0zm26.8 2.86v-8.58q0 0 0 0h16.76q0 0 0 0h0v8.58h0zm26.8-20.02V91.52q0 0 0 0h16.76q0 0 0 0h0v22.88h0zm26.8 2.86V97.24q0 0 0 0h16.76q0 0 0 0h0v20.02h0zm26.8-57.2V25.74q0 0 0 0h16.76q0 0 0 0h0v34.32h0zm26.8 28.6V42.9q0 0 0 0h16.76q0 0 0 0h0v45.76h0zm26.8-17.16V5.72q0 0 0 0h16.76q0 0 0 0h0V71.5h0zm26.8 45.76V97.24q0 0 0 0h16.76q0 0 0 0h0v20.02h0zm26.8-8.58V77.22q0 0 0 0h16.76q0 0 0 0h0v31.46h0zm26.8 14.3v-14.3q0 0 0 0h16.76q0 0 0 0h0v14.3h0z"
            className="column-chart-preview_svg__apexcharts-bar-area"
            clipPath="url(#column-chart-preview_svg__b)"
          />
        </g>
      </g>
      <path
        stroke="#b6b6b6"
        strokeDasharray={0}
        className="column-chart-preview_svg__apexcharts-ycrosshairs"
        d="M0 0h268"
      />
      <path
        className="column-chart-preview_svg__apexcharts-ycrosshairs-hidden"
        d="M0 0h268"
      />
    </g>
  </svg>
);

export default SvgColumnChartPreview;
