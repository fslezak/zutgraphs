import * as React from 'react';
import { SVGProps } from 'react';

const SvgBarChartPreview = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width={300}
    height={188}
    xmlns="http://www.w3.org/2000/svg"
    className="bar-chart-preview_svg__apexcharts-svg"
    style={{
      background: '0 0',
    }}
    {...props}
  >
    <g
      className="bar-chart-preview_svg__apexcharts-inner bar-chart-preview_svg__apexcharts-graphical"
      transform="translate(27 30)"
    >
      <defs>
        <clipPath id="bar-chart-preview_svg__a">
          <rect
            width={269}
            height={145}
            x={-3}
            y={-1}
            rx={0}
            ry={0}
            strokeWidth={0}
            strokeDasharray={0}
            fill="#fff"
          />
        </clipPath>
      </defs>
      <g className="bar-chart-preview_svg__apexcharts-grid" strokeDasharray={0}>
        <g
          className="bar-chart-preview_svg__apexcharts-gridlines-horizontal"
          stroke="#f1f1f1"
        >
          <path
            className="bar-chart-preview_svg__apexcharts-gridline"
            d="M0 0h263M0 20.429h263M0 40.857h263M0 61.286h263M0 81.714h263M0 102.143h263M0 122.571h263M0 143h263"
          />
        </g>
        <path stroke="transparent" d="M0 143h263M0 1v142" />
      </g>
      <g
        className="bar-chart-preview_svg__apexcharts-bar-series bar-chart-preview_svg__apexcharts-plot-series"
        strokeLinecap="round"
        strokeWidth={2}
        strokeDasharray={0}
      >
        <g
          className="bar-chart-preview_svg__apexcharts-series"
          fill="rgba(0,143,251,0.85)"
          stroke="#008ffb"
        >
          <path
            d="M.1 3.064h21.04q0 0 0 0v12.3q0 0 0 0h0H.1h0zm0 20.429h15.78q0 0 0 0v12.3q0 0 0 0h0H.1h0zm0 20.428h52.6q0 0 0 0v12.3q0 0 0 0h0H.1h0zm0 20.429h47.34q0 0 0 0v12.3q0 0 0 0h0H.1h0zm0 20.429h152.54q0 0 0 0v12.3q0 0 0 0h0H.1h0zm0 20.428h99.94q0 0 0 0v12.3q0 0 0 0h0H.1h0zm0 20.429h131.5q0 0 0 0v12.3q0 0 0 0h0H.1h0z"
            className="bar-chart-preview_svg__apexcharts-bar-area"
            clipPath="url(#bar-chart-preview_svg__a)"
          />
        </g>
        <g
          className="bar-chart-preview_svg__apexcharts-series"
          fill="rgba(0,227,150,0.85)"
          stroke="#00e396"
        >
          <path
            d="M21.14 3.064h10.52q0 0 0 0v12.3q0 0 0 0h0-10.52 0zm-5.26 20.429h15.78q0 0 0 0v12.3q0 0 0 0h0-15.78 0zM52.7 43.921h42.08q0 0 0 0v12.3q0 0 0 0h0H52.7h0zM47.44 64.35h36.82q0 0 0 0v12.3q0 0 0 0h0-36.82 0zm105.2 20.429h63.12q0 0 0 0v12.3q0 0 0 0h0-63.12 0zm-52.6 20.428h84.16q0 0 0 0v12.3q0 0 0 0h0-84.16 0zm31.56 20.429h120.98q0 0 0 0v12.3q0 0 0 0h0H131.6h0z"
            className="bar-chart-preview_svg__apexcharts-bar-area"
            clipPath="url(#bar-chart-preview_svg__a)"
          />
        </g>
      </g>
      <path
        stroke="#b6b6b6"
        strokeDasharray={0}
        className="bar-chart-preview_svg__apexcharts-ycrosshairs"
        d="M0 0h263"
      />
      <path
        className="bar-chart-preview_svg__apexcharts-ycrosshairs-hidden"
        d="M0 0h263"
      />
    </g>
  </svg>
);

export default SvgBarChartPreview;
