import * as React from 'react';
import { SVGProps } from 'react';

const SvgAreaChartPreview = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width={300}
    height={188}
    xmlns="http://www.w3.org/2000/svg"
    className="area-chart-preview_svg__apexcharts-svg area-chart-preview_svg__apexcharts-zoomable"
    style={{
      background: '0 0',
    }}
    {...props}
  >
    <g
      className="area-chart-preview_svg__apexcharts-inner area-chart-preview_svg__apexcharts-graphical"
      transform="translate(22 30)"
    >
      <defs>
        <linearGradient
          id="area-chart-preview_svg__c"
          x1={0}
          y1={0}
          x2={0}
          y2={1}
        >
          <stop
            stopOpacity={0.65}
            stopColor="rgba(0,143,251,0.65)"
            offset={0}
          />
          <stop
            stopOpacity={0.5}
            stopColor="rgba(128,199,253,0.5)"
            offset={1}
          />
          <stop
            stopOpacity={0.5}
            stopColor="rgba(128,199,253,0.5)"
            offset={1}
          />
        </linearGradient>
        <linearGradient
          id="area-chart-preview_svg__a"
          x1={0}
          y1={0}
          x2={0}
          y2={1}
        >
          <stop
            stopOpacity={0.65}
            stopColor="rgba(0,227,150,0.65)"
            offset={0}
          />
          <stop
            stopOpacity={0.5}
            stopColor="rgba(128,241,203,0.5)"
            offset={1}
          />
          <stop
            stopOpacity={0.5}
            stopColor="rgba(128,241,203,0.5)"
            offset={1}
          />
        </linearGradient>
        <clipPath id="area-chart-preview_svg__b">
          <rect
            width={274}
            height={145}
            x={-3}
            y={-1}
            rx={0}
            ry={0}
            strokeWidth={0}
            strokeDasharray={0}
            fill="#fff"
          />
        </clipPath>
      </defs>
      <path
        stroke="#b6b6b6"
        strokeDasharray={3}
        className="area-chart-preview_svg__apexcharts-xcrosshairs"
        fill="#b1b9c4"
        filter="none"
        fillOpacity={0.9}
        d="M0 0v143"
      />
      <g
        className="area-chart-preview_svg__apexcharts-grid"
        strokeDasharray={0}
      >
        <g
          className="area-chart-preview_svg__apexcharts-gridlines-horizontal"
          stroke="#f1f1f1"
        >
          <path
            className="area-chart-preview_svg__apexcharts-gridline"
            d="M0 0h268M0 28.6h268M0 57.2h268M0 85.8h268M0 114.4h268M0 143h268"
          />
        </g>
        <path stroke="transparent" d="M0 143h268M0 1v142" />
      </g>
      <g className="area-chart-preview_svg__apexcharts-area-series area-chart-preview_svg__apexcharts-plot-series">
        <g className="area-chart-preview_svg__apexcharts-series">
          <path
            d="M0 143v-17.16h29.778c10.422 0 19.355-34.32 29.778-34.32 10.422 0 19.355 5.72 29.777 5.72 10.423 0 19.356-71.5 29.778-71.5 10.422 0 19.356 17.16 29.778 17.16 10.422 0 19.355-37.18 29.778-37.18 10.422 0 19.355 91.52 29.777 91.52 10.423 0 19.356-20.02 29.778-20.02 10.422 0 19.356 31.46 29.778 31.46V143m0-34.32z"
            fill="url(#area-chart-preview_svg__a)"
            className="area-chart-preview_svg__apexcharts-area"
            clipPath="url(#area-chart-preview_svg__b)"
          />
          <path
            d="M0 125.84h29.778c10.422 0 19.355-34.32 29.778-34.32 10.422 0 19.355 5.72 29.777 5.72 10.423 0 19.356-71.5 29.778-71.5 10.422 0 19.356 17.16 29.778 17.16 10.422 0 19.355-37.18 29.778-37.18 10.422 0 19.355 91.52 29.777 91.52 10.423 0 19.356-20.02 29.778-20.02 10.422 0 19.356 31.46 29.778 31.46"
            fill="none"
            stroke="#00e396"
            strokeWidth={2}
            strokeDasharray={0}
            className="area-chart-preview_svg__apexcharts-area"
            clipPath="url(#area-chart-preview_svg__b)"
          />
        </g>
        <g className="area-chart-preview_svg__apexcharts-series">
          <path
            d="M0 143v-11.44c10.422 0 19.356 2.86 29.778 2.86 10.422 0 19.355-20.02 29.778-20.02 10.422 0 19.355 2.86 29.777 2.86 10.423 0 19.356-57.2 29.778-57.2 10.422 0 19.356 28.6 29.778 28.6 10.422 0 19.355-17.16 29.778-17.16 10.422 0 19.355 45.76 29.777 45.76 10.423 0 19.356-8.58 29.778-8.58 10.422 0 19.356 14.3 29.778 14.3V143m0-20.02z"
            fill="url(#area-chart-preview_svg__c)"
            className="area-chart-preview_svg__apexcharts-area"
            clipPath="url(#area-chart-preview_svg__b)"
          />
          <path
            d="M0 131.56c10.422 0 19.356 2.86 29.778 2.86 10.422 0 19.355-20.02 29.778-20.02 10.422 0 19.355 2.86 29.777 2.86 10.423 0 19.356-57.2 29.778-57.2 10.422 0 19.356 28.6 29.778 28.6 10.422 0 19.355-17.16 29.778-17.16 10.422 0 19.355 45.76 29.777 45.76 10.423 0 19.356-8.58 29.778-8.58 10.422 0 19.356 14.3 29.778 14.3"
            fill="none"
            stroke="#008ffb"
            strokeWidth={2}
            strokeDasharray={0}
            className="area-chart-preview_svg__apexcharts-area"
            clipPath="url(#area-chart-preview_svg__b)"
          />
        </g>
      </g>
      <path
        stroke="#b6b6b6"
        strokeDasharray={0}
        className="area-chart-preview_svg__apexcharts-ycrosshairs"
        d="M0 0h268"
      />
      <path
        className="area-chart-preview_svg__apexcharts-ycrosshairs-hidden"
        d="M0 0h268"
      />
    </g>
  </svg>
);

export default SvgAreaChartPreview;
