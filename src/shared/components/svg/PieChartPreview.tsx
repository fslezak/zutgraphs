import * as React from 'react';
import { SVGProps } from 'react';

const SvgPieChartPreview = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width={180}
    height={158.7}
    xmlns="http://www.w3.org/2000/svg"
    className="pie-chart-preview_svg__apexcharts-svg"
    transform="translate(0 10)"
    style={{
      background: '0 0',
    }}
    {...props}
  >
    <g className="pie-chart-preview_svg__apexcharts-inner pie-chart-preview_svg__apexcharts-graphical">
      <g
        transform="translate(13)"
        className="pie-chart-preview_svg__apexcharts-pie"
      >
        <circle r={45.563} cx={78} cy={78} fill="transparent" />
        <g className="pie-chart-preview_svg__apexcharts-slices">
          <g className="pie-chart-preview_svg__apexcharts-series pie-chart-preview_svg__apexcharts-pie-series">
            <path
              d="M78 7.902a70.098 70.098 0 0 1 64.457 97.648l-22.56-9.642A45.563 45.563 0 0 0 78 32.437V7.902z"
              fill="rgba(0,143,251,1)"
              strokeWidth={2}
              strokeDasharray={0}
              className="pie-chart-preview_svg__apexcharts-pie-area pie-chart-preview_svg__apexcharts-donut-slice-0"
              stroke="#fff"
            />
          </g>
          <g className="pie-chart-preview_svg__apexcharts-series pie-chart-preview_svg__apexcharts-pie-series">
            <path
              d="M142.457 105.55a70.098 70.098 0 0 1-132.028-8.9l23.65-6.527a45.563 45.563 0 0 0 85.818 5.786l22.56 9.642z"
              fill="rgba(0,227,150,1)"
              strokeWidth={2}
              strokeDasharray={0}
              className="pie-chart-preview_svg__apexcharts-pie-area pie-chart-preview_svg__apexcharts-donut-slice-1"
              stroke="#fff"
            />
          </g>
          <g className="pie-chart-preview_svg__apexcharts-series pie-chart-preview_svg__apexcharts-pie-series">
            <path
              d="M10.429 96.649A70.098 70.098 0 0 1 77.988 7.902l.004 24.535a45.563 45.563 0 0 0-43.913 57.685l-23.65 6.527z"
              fill="rgba(254,176,25,1)"
              strokeWidth={2}
              strokeDasharray={0}
              className="pie-chart-preview_svg__apexcharts-pie-area pie-chart-preview_svg__apexcharts-donut-slice-2"
              stroke="#fff"
            />
          </g>
        </g>
      </g>
      <path
        stroke="#b6b6b6"
        strokeDasharray={0}
        className="pie-chart-preview_svg__apexcharts-ycrosshairs"
        d="M13 0h156"
      />
      <path
        className="pie-chart-preview_svg__apexcharts-ycrosshairs-hidden"
        d="M13 0h156"
      />
    </g>
  </svg>
);

export default SvgPieChartPreview;
