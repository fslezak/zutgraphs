import * as React from 'react';
import { SVGProps } from 'react';

const SvgLineChartPreview = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width={300}
    height={188}
    xmlns="http://www.w3.org/2000/svg"
    className="line-chart-preview_svg__apexcharts-svg line-chart-preview_svg__apexcharts-zoomable"
    style={{
      background: '0 0',
    }}
    {...props}
  >
    <g
      className="line-chart-preview_svg__apexcharts-inner line-chart-preview_svg__apexcharts-graphical"
      transform="translate(22 30)"
    >
      <defs>
        <clipPath id="line-chart-preview_svg__a">
          <rect
            width={274}
            height={145}
            x={-3}
            y={-1}
            rx={0}
            ry={0}
            strokeWidth={0}
            strokeDasharray={0}
            fill="#fff"
          />
        </clipPath>
      </defs>
      <path
        stroke="#b6b6b6"
        strokeDasharray={3}
        className="line-chart-preview_svg__apexcharts-xcrosshairs"
        fill="#b1b9c4"
        filter="none"
        fillOpacity={0.9}
        d="M0 0v143"
      />
      <g
        className="line-chart-preview_svg__apexcharts-grid"
        strokeDasharray={0}
      >
        <g
          className="line-chart-preview_svg__apexcharts-gridlines-horizontal"
          stroke="#f1f1f1"
        >
          <path
            className="line-chart-preview_svg__apexcharts-gridline"
            d="M0 0h268M0 28.6h268M0 57.2h268M0 85.8h268M0 114.4h268M0 143h268"
          />
        </g>
        <path stroke="transparent" d="M0 143h268M0 1v142" />
      </g>
      <g className="line-chart-preview_svg__apexcharts-line-series line-chart-preview_svg__apexcharts-plot-series">
        <g className="line-chart-preview_svg__apexcharts-series">
          <path
            d="m0 77.22 29.778 8.58 29.778-25.74 29.777 28.6 29.778-61.49 29.778 54.34 29.778 7.15 29.777-25.74 29.778 24.31L268 82.94"
            fill="none"
            stroke="rgba(0,227,150,0.85)"
            strokeWidth={2}
            strokeDasharray={0}
            className="line-chart-preview_svg__apexcharts-line"
            clipPath="url(#line-chart-preview_svg__a)"
          />
        </g>
        <g className="line-chart-preview_svg__apexcharts-series">
          <path
            d="m0 80.08 29.778 1.43L59.556 71.5l29.777 27.17 29.778-54.34 29.778 14.3 29.778 62.92 29.777-48.62 29.778 30.03L268 75.79"
            fill="none"
            stroke="rgba(0,143,251,0.85)"
            strokeWidth={2}
            strokeDasharray={0}
            className="line-chart-preview_svg__apexcharts-line"
            clipPath="url(#line-chart-preview_svg__a)"
          />
        </g>
      </g>
      <path
        stroke="#b6b6b6"
        strokeDasharray={0}
        className="line-chart-preview_svg__apexcharts-ycrosshairs"
        d="M0 0h268"
      />
      <path
        className="line-chart-preview_svg__apexcharts-ycrosshairs-hidden"
        d="M0 0h268"
      />
    </g>
  </svg>
);

export default SvgLineChartPreview;
