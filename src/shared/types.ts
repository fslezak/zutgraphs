/* eslint-disable @typescript-eslint/no-explicit-any */

export interface ParsedFileData {
  headers: string[];
  rows: string[][];
}

export interface DataConfig {
  [key: string]: ColumnConfig;
}

export interface ChartData {
  [key: string]: unknown;
}

export interface LineChartConfig {
  data: ChartData[];
  xKey: string;
  yConfigs: {
    dataKey: string;
  }[];
}

export type ChartConfig = LineChartConfig | undefined;

export type ColumnDataType =
  | 'category'
  | 'datetime'
  | 'numeric'
  | 'string'
  | undefined;

export interface ColumnConfig {
  dataType?: ColumnDataType;
  data: unknown[];
  is_x: boolean;
  is_hidden: boolean;
  date_format?: string;
  float_separator?: string;
  float_precision?: number;
  label?: string;
}
