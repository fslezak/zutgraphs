import './shared/scss/style.scss';

import { MantineProvider } from '@mantine/core';
import { NotificationsProvider } from '@mantine/notifications';
import { createRoot } from 'react-dom/client';

import { App } from './App';

const container = document.getElementById('root');
if (!container) {
  throw Error('No root element!');
}
const root = createRoot(container);
root.render(
  <MantineProvider>
    <NotificationsProvider position="top-right">
      <App />
    </NotificationsProvider>
  </MantineProvider>
);
