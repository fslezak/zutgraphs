import { useMemo } from 'react';

import {
  AreaChartPreview,
  BarChartPreview,
  ColumnChartPreview,
  LineChartPreview,
  PieChartPreview,
  ScatterChartPreview,
} from '../../../shared/components/svg';
import { PreviewContainer } from './PreviewContainer';

export enum ChartType {
  LINE = 'line',
  AREA = 'area',
  COLUMN = 'column',
  BAR = 'bar',
  SCATTER = 'scatter',
  PIE = 'pie',
}

interface Props {
  onPick: (v: ChartType) => void;
  availableCharts?: ChartType[];
}

interface ChartPreview {
  preview: JSX.Element;
  type: ChartType;
  label: string;
}

const charts: ChartPreview[] = [
  {
    label: 'Line',
    preview: <LineChartPreview />,
    type: ChartType.LINE,
  },
  {
    label: 'Area',
    preview: <AreaChartPreview />,
    type: ChartType.AREA,
  },
  {
    label: 'Column',
    preview: <ColumnChartPreview />,
    type: ChartType.COLUMN,
  },
  {
    label: 'Bar',
    preview: <BarChartPreview />,
    type: ChartType.BAR,
  },
  {
    label: 'Scatter',
    type: ChartType.SCATTER,
    preview: <ScatterChartPreview />,
  },
  {
    label: 'Pie',
    type: ChartType.PIE,
    preview: <PieChartPreview />,
  },
];

export const ChartTypePicker: React.FC<Props> = ({
  onPick,
  availableCharts,
}) => {
  const filteredCharts = useMemo(() => {
    if (!availableCharts) {
      return charts;
    }
    return charts.filter((c) => availableCharts.includes(c.type));
  }, [availableCharts]);

  return (
    <div className="previews-container">
      {filteredCharts.map((c) => (
        <PreviewContainer
          onClick={() => onPick(c.type)}
          key={c.label}
          label={c.label}
          preview={c.preview}
        />
      ))}
    </div>
  );
};
