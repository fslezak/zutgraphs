import './style.scss';

import { Modal } from '@mantine/core';

import { ChartType, ChartTypePicker } from './ChartTypePicker';

interface Props {
  isOpen: boolean;
  setIsOpen: (b: boolean) => void;
  onPick: (chartType: ChartType) => void;
  availableCharts?: ChartType[];
}

export const ChartTypeModal: React.FC<Props> = ({
  isOpen,
  setIsOpen,
  onPick,
  availableCharts,
}) => {
  return (
    <Modal
      opened={isOpen}
      onClose={() => setIsOpen(false)}
      title="Wybierz wykres"
      size="full"
      className="pick-chart-type"
      overflow="inside"
      centered
    >
      <ChartTypePicker
        availableCharts={availableCharts}
        onPick={(v) => {
          onPick(v);
          setIsOpen(false);
        }}
      />
    </Modal>
  );
};
