import { HTMLMotionProps, motion, Variants } from 'framer-motion';
import { ReactElement } from 'react';

interface Props extends HTMLMotionProps<'div'> {
  preview: ReactElement;
  label: string;
}

export const PreviewContainer: React.FC<Props> = ({
  preview,
  label,
  ...rest
}) => {
  return (
    <motion.div
      className="preview-container"
      whileTap={{
        scale: 0.9,
      }}
      variants={containerVariants}
      initial="idle"
      animate="idle"
      whileHover="hover"
      {...rest}
    >
      <div className="preview">{preview}</div>
      <div className="footer">
        <p>{label}</p>
      </div>
    </motion.div>
  );
};

const containerVariants: Variants = {
  idle: {
    y: 0,
  },
  hover: {
    y: -10,
  },
};
