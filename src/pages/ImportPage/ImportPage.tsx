import './style.scss';

import { Button, Divider, Select, SelectItem } from '@mantine/core';
import { isUndefined } from 'lodash-es';
import { useCallback, useEffect, useState } from 'react';
import {
  Legend,
  Line,
  LineChart,
  ResponsiveContainer,
  XAxis,
  YAxis,
} from 'recharts';

import {
  DataConfig,
  LineChartConfig,
  ParsedFileData,
} from '../../shared/types';
import { ChartDataDefinitionsPicker } from './ChartDataDefinitionsPicker/ChartDataDefinitionsPicker';
import { ChartTypeModal } from './ChartTypeModal/ChartTypeModal';
import { ChartType } from './ChartTypeModal/ChartTypePicker';
import { parseDataConfig, parseFile, validateDataConfig } from './Importer';
import { ImportFile } from './ImportFile/ImportFile';
import { PreviewTable } from './PreviewTable/PreviewTable';

export const ImportPage: React.FC = () => {
  const [chartTypeModalOpen, setChartTypeModalOpen] = useState(false);
  const [rawFileContent, setRawFileContent] = useState('');
  const [parsedFileContent, setParsedFileContent] = useState<
    ParsedFileData | undefined
  >();
  useState();
  const [pickedChartType, setPickedChartType] = useState<ChartType | undefined>(
    undefined
  );

  const [columnSeparator, setColumnSeparator] = useState(',');
  const [rowSeparator, setRowSeparator] = useState('\n');

  const [previewChartProps, setPreviewChartProps] = useState<LineChartConfig>();
  const [chartDataConfig, setChartDataConfig] = useState<
    DataConfig | undefined
  >();
  const [availableCharts, setAvailableCharts] = useState<ChartType[]>();

  useEffect(() => {
    if (columnSeparator && rowSeparator && rawFileContent) {
      setParsedFileContent(
        parseFile(rowSeparator, columnSeparator, rawFileContent)
      );
      setChartDataConfig(undefined);
      setPickedChartType(undefined);
    } else {
      setChartDataConfig(undefined);
      setPickedChartType(undefined);
    }
  }, [columnSeparator, rawFileContent, rowSeparator]);

  useEffect(() => {
    if (pickedChartType && chartDataConfig) {
      const parsed = parseDataConfig(chartDataConfig, ChartType.LINE);
      setPreviewChartProps(parsed);
    } else {
      if (previewChartProps) {
        setPreviewChartProps(undefined);
      }
    }
  }, [chartDataConfig, pickedChartType, previewChartProps]);

  const handleDataConfigChange = useCallback((data?: DataConfig) => {
    if (data) {
      setChartDataConfig(data);
      setAvailableCharts(validateDataConfig(data));
    }
    setPickedChartType(undefined);
  }, []);

  const handleChartTypePick = useCallback(
    (t: ChartType) => {
      setPickedChartType(t);
      if (chartDataConfig) {
        const parsed = parseDataConfig(chartDataConfig, ChartType.LINE);
        setPreviewChartProps(parsed);
      }
    },
    [chartDataConfig]
  );

  return (
    <>
      <div id="import-page">
        <ImportFile onFileLoad={(raw) => setRawFileContent(raw)} />
        <div className="grid">
          <div className="side-left">
            <Select
              label="Separator wierszy"
              data={separatorsOptions}
              placeholder="Wybierz separator"
              disabled={!rawFileContent}
              nothingFound="Brak elementów"
              onChange={(val) => {
                if (val) {
                  setRowSeparator(val);
                }
              }}
              value={rowSeparator}
            />
            <Select
              label="Separator kolumn"
              data={separatorsOptions}
              placeholder="Wybierz separator"
              disabled={!rawFileContent}
              nothingFound="Brak elementów"
              value={columnSeparator}
              onChange={(val) => {
                if (val) {
                  setColumnSeparator(val);
                }
              }}
            />
            <Divider
              orientation="horizontal"
              variant="solid"
              my="sm"
              label="Definicje danych"
              labelPosition="center"
            />
            {parsedFileContent && (
              <ChartDataDefinitionsPicker
                parsedData={parsedFileContent}
                onConfigurationChange={handleDataConfigChange}
              />
            )}
            <Divider
              orientation="horizontal"
              variant="solid"
              my="sm"
              label="Typ wykresu"
              labelPosition="center"
            />
            <div className="chart-type-picker">
              {pickedChartType && (
                <p>
                  Wybrany typ: <span>{toTitleCase(pickedChartType)}</span>
                </p>
              )}
              <Button
                onClick={() => setChartTypeModalOpen(true)}
                loading={chartTypeModalOpen}
                disabled={
                  (isUndefined(chartDataConfig) && !availableCharts) ||
                  (availableCharts && availableCharts.length === 0)
                }
              >
                {pickedChartType ? 'Zmień' : 'Wybierz'} typ
              </Button>
            </div>
          </div>
          <div className="side-right">
            {parsedFileContent && <PreviewTable data={parsedFileContent} />}
            {previewChartProps && (
              <ResponsiveContainer width="100%" height={400}>
                <LineChart
                  data={previewChartProps.data}
                  width={500}
                  height={300}
                >
                  <XAxis dataKey={previewChartProps.xKey} />
                  <YAxis />
                  <Legend />
                  {previewChartProps.yConfigs.map((config) => (
                    <Line
                      dataKey={config.dataKey}
                      key={config.dataKey}
                      type="monotone"
                      stroke="#8884d8"
                    />
                  ))}
                </LineChart>
              </ResponsiveContainer>
            )}
          </div>
        </div>
      </div>
      <ChartTypeModal
        isOpen={chartTypeModalOpen}
        setIsOpen={setChartTypeModalOpen}
        onPick={handleChartTypePick}
        availableCharts={availableCharts}
      />
    </>
  );
};

const toTitleCase = (str: string): string => {
  let upper = true;
  let newStr = '';
  for (let i = 0, l = str.length; i < l; i++) {
    if (str[i] == ' ') {
      upper = true;
      newStr += str[i];
      continue;
    }
    newStr += upper ? str[i].toUpperCase() : str[i].toLowerCase();
    upper = false;
  }
  return newStr;
};

const separatorsOptions: SelectItem[] = [
  {
    value: '\n',
    label: 'Nowa linia',
  },
  {
    value: ',',
    label: 'Przecinek',
  },
  {
    value: '.',
    label: 'Kropka',
  },
  {
    value: ' ',
    label: 'Spacja',
  },
  {
    value: ';',
    label: 'Średnik (;)',
  },
  {
    value: ':',
    label: 'Dwukropek',
  },
];
