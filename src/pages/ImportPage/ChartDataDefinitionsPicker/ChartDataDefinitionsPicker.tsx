import './style.scss';

import { Checkbox, Select, SelectItem } from '@mantine/core';
import { cloneDeep, isUndefined, pickBy } from 'lodash-es';
import { useCallback, useEffect, useState } from 'react';

import {
  ColumnConfig,
  DataConfig,
  ParsedFileData,
} from '../../../shared/types';

interface Props {
  parsedData: ParsedFileData;
  onConfigurationChange: (configuration?: DataConfig) => void;
}

export const ChartDataDefinitionsPicker: React.FC<Props> = ({
  onConfigurationChange,
  parsedData,
}) => {
  const [configStates, setConfigStates] = useState<DataConfig>();
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [dateFormat, setDateFormat] = useState('');

  // Fill With defaults at raw or config reload.
  useEffect(() => {
    const initialConfig: DataConfig = {};
    parsedData.headers.forEach((header, index) => {
      initialConfig[header] = {
        data: parsedData.rows.map((r) => r[index]),
        dataType: undefined,
        is_hidden: false,
        is_x: false,
        date_format: undefined,
        float_precision: undefined,
        float_separator: undefined,
        label: undefined,
      };
    });
    setConfigStates(initialConfig);
    onConfigurationChange(undefined);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [parsedData.headers, parsedData.rows]);

  // Validates current config and sends it back
  useEffect(() => {
    if (configStates) {
      let configValid = true;
      let xPresent = false;
      Object.keys(configStates).forEach((key) => {
        const config = configStates[key];
        if (!config.dataType) {
          configValid = false;
        }
        if (config.is_x) {
          xPresent = true;
        }
        if (
          config.dataType === 'numeric' &&
          isUndefined(config.float_separator)
        ) {
          configValid = false;
        }
      });
      if (!xPresent) {
        configValid = false;
      }
      const toSend = pickBy(configStates, (config) => !config.is_hidden);
      if (Object.keys(toSend).length < 1) {
        configValid = false;
      }
      if (configValid) {
        onConfigurationChange(toSend);
      } else {
        onConfigurationChange(undefined);
      }
    }
  }, [configStates, onConfigurationChange]);

  const getConfigState = useCallback(
    (header: string) => {
      if (configStates) {
        return configStates[header];
      }
      return undefined;
    },
    [configStates]
  );

  const setConfigType = useCallback(
    (header: string, option?: string) => {
      if (configStates) {
        const clone = cloneDeep(configStates);
        let config = configStates[header];
        config = {
          ...config,
          dataType: option,
          date_format: undefined,
          float_precision: undefined,
          label: undefined,
          float_separator: undefined,
        } as ColumnConfig;
        clone[header] = config;
        setConfigStates(clone);
      }
    },
    [configStates]
  );

  const setFloatSeparator = useCallback(
    (header: string, option?: string) => {
      if (configStates) {
        const clone = cloneDeep(configStates);
        let config = configStates[header];
        if (option === '') {
          config = {
            ...config,
            float_precision: undefined,
            float_separator: '',
          };
        } else {
          config = { ...config, float_separator: option };
        }
        clone[header] = config;
        setConfigStates(clone);
      }
    },
    [configStates]
  );

  const setXColumn = useCallback(
    (header: string) => {
      if (configStates) {
        const cloned = cloneDeep(configStates);
        Object.keys(cloned).forEach((key) => {
          if (key !== header) {
            cloned[key].is_x = false;
          }
        });
        cloned[header].is_x = !cloned[header].is_x;
        setConfigStates(cloned);
      }
    },
    [configStates]
  );

  const setHidden = useCallback(
    (header: string) => {
      if (configStates) {
        const cloned = cloneDeep(configStates);
        const config = configStates[header];
        config.is_hidden = !config.is_hidden;
        cloned[header] = config;
        setConfigStates(cloned);
      }
    },
    [configStates]
  );

  if (!configStates) return null;

  return (
    <>
      {parsedData.headers.map((header, index) => {
        const configState = getConfigState(header);
        const selectedDataType = configState?.dataType;
        const selectedFloatSeparator = configState?.float_separator;
        const showNumeric = selectedDataType === 'numeric';

        return (
          <div className="header-config" key={header + index}>
            <label>{header}:</label>
            <Select
              data={configTypesOptions}
              value={selectedDataType}
              onChange={(val) => setConfigType(header, val ?? 'string')}
              placeholder="Wybierz typ danych"
            />
            {showNumeric && (
              <Select
                data={floatSeparatorOptions}
                value={selectedFloatSeparator}
                onChange={(val) => setFloatSeparator(header, val ?? '')}
                placeholder="Wybierz separator dziesiętny"
              />
            )}
            <Checkbox
              checked={configState?.is_x}
              onChange={() => setXColumn(header)}
              label="Wyznacznik osi X"
            />
            <Checkbox
              checked={configState?.is_hidden}
              onChange={() => setHidden(header)}
              label="Ukryta"
            />
          </div>
        );
      })}
    </>
  );
};

const configTypesOptions: SelectItem[] = [
  {
    value: 'numeric',
    label: 'Numeryczny',
  },
  {
    value: 'datetime',
    label: 'Data',
  },
  {
    value: 'categories',
    label: 'Kategorie',
  },
  {
    value: 'string',
    label: 'Ciąg znaków',
  },
];

const floatSeparatorOptions: SelectItem[] = [
  {
    value: ',',
    label: 'Przecinek',
  },
  {
    value: '.',
    label: 'Kropka',
  },
  {
    value: 'int',
    label: 'Liczba całkowita',
  },
];
