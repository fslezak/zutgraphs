import './style.scss';

import { Button, Input } from '@mantine/core';
import { showNotification } from '@mantine/notifications';
import { fs } from '@tauri-apps/api';
import { open } from '@tauri-apps/api/dialog';
import { useState } from 'react';

interface Props {
  onFileLoad: (s: string) => void;
}

export const ImportFile: React.FC<Props> = ({ onFileLoad }) => {
  const [filePath, setFilePath] = useState<string>('');
  const [fileImportLoading, setFileImportLoading] = useState(false);
  const handleImportClick = async () => {
    setFileImportLoading(true);
    open({
      filters: [{ extensions: ['txt'], name: '.txt' }],
    })
      .then(async (filePath) => {
        if (!Array.isArray(filePath) && filePath) {
          setFilePath(filePath);
          const fileContent = await fs.readTextFile(filePath);
          onFileLoad(fileContent);
        } else {
          if (Array.isArray(filePath)) {
            showNotification({
              message: 'Nie można importować wielu źródeł danych na raz !',
              color: 'red',
              autoClose: 5000,
            });
          } else {
            showNotification({
              message: 'Nie udało się pobrać ścieżki !',
              color: 'yellow',
              autoClose: 5000,
            });
          }
        }
      })
      .finally(() => {
        setFileImportLoading(false);
      });
  };

  return (
    <div className="import-container">
      <Input value={filePath} disabled />
      <Button onClick={handleImportClick} loading={fileImportLoading}>
        {filePath && filePath.length ? 'Zmień plik' : 'Wybierz plik'}
      </Button>
    </div>
  );
};
