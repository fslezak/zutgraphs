import { clone, isUndefined, range, toNumber } from 'lodash-es';

import {
  ChartConfig,
  ChartData,
  DataConfig,
  ParsedFileData,
} from '../../shared/types';
import { ChartType } from './ChartTypeModal/ChartTypePicker';

export const parseFile = (
  rowSeparator: string,
  columnSeparator: string,
  rawFileContent: string
): ParsedFileData => {
  const fileLines = rawFileContent.split(rowSeparator);
  const headers = fileLines[0].split(columnSeparator);
  const rows = fileLines.splice(1).map((row) => row.split(columnSeparator));
  return {
    headers,
    rows,
  };
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type EmptyInit = { [key: string]: any };

export const parseDataConfig = (
  config: DataConfig,
  type: ChartType
): ChartConfig => {
  switch (type) {
    case ChartType.LINE:
      // Make data
      let max = 0;
      const empty: EmptyInit = {};
      Object.keys(config).forEach((column) => {
        empty[column] = null;
        if (max + 1 < config[column].data.length) {
          max = config[column].data.length;
        }
      });
      // Init empty data set with correct keys
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const data: any[] = range(0, max).map(() => clone(empty));
      // Fill data
      Object.keys(config).forEach((column) => {
        config[column].data.forEach((val, index) => {
          const columnConfig = config[column];
          const rowValue = val as string;
          if (config[column].dataType === 'numeric') {
            // parse row data (raw is always string) to number
            if (
              isUndefined(columnConfig.float_separator) ||
              columnConfig.float_separator === ''
            ) {
              data[index][column] = toNumber(rowValue);
            } else {
              if (
                columnConfig.float_separator &&
                columnConfig.float_separator === ','
              ) {
                data[index][column] = parseFloat(rowValue.replaceAll(',', '.'));
              } else {
                data[index][column] = parseFloat(rowValue);
              }
            }
          } else {
            data[index][column] = val;
          }
        });
      });
      // Make rest of the config
      // Get xAxis key
      const xKey = Object.keys(config).find((column) => config[column].is_x);
      if (!xKey) {
        throw Error('X Axis undefined ');
      }
      const chartData = data as ChartData[];
      return {
        xKey,
        data: chartData,
        yConfigs: Object.keys(config)
          .filter((col) => !config[col].is_x)
          .map((col) => ({
            dataKey: col,
          })),
      };
    default:
      return undefined;
  }
};

const validateLineConfig = (data: DataConfig): boolean => {
  let xAxisPresent = false;
  const visibleColumns: string[] = [];
  let visibleLinesCount = 0;
  Object.keys(data).forEach((column) => {
    if (!data[column].is_hidden) {
      visibleColumns.push(column);
    }
  });
  for (const column of visibleColumns) {
    const config = data[column];
    if (config.is_x) {
      xAxisPresent = true;
    } else {
      visibleLinesCount += 1;
    }
  }
  if (!xAxisPresent) {
    return false;
  }
  if (visibleLinesCount < 1) {
    return false;
  }
  return true;
};

export const validateDataConfig = (data: DataConfig): ChartType[] => {
  const res: ChartType[] = [];
  const line_valid = validateLineConfig(data);
  if (line_valid) {
    res.push(ChartType.LINE);
  }
  return res;
};
