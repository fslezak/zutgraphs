import { ParsedFileData } from '../../../shared/types';

interface Props {
  data: ParsedFileData;
}

export const PreviewTable: React.FC<Props> = ({ data }) => {
  return (
    <table>
      <thead>
        <tr>
          {data.headers.map((header) => (
            <th key={header}>{header}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {data.rows.map((row, index) => (
          <tr key={index}>
            {row.map((v, index) => (
              <td key={index}>{v}</td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
};
