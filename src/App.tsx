import { ImportPage } from './pages/ImportPage/ImportPage';

export const App = () => {
  return (
    <div className="app-container">
      <ImportPage />
    </div>
  );
};
