# ZutGraphs

## Developer Environment Setup

### Dependencies

- [Node](https://nodejs.org/en/download/)
- [Rust](https://www.rust-lang.org/) (Needs to be stable)
- [Source code](https://gitlab.com/fslezak/zutgraphs)

Next, navigate to root directory of downloaded project.

You need will need [Yarn](https://classic.yarnpkg.com/lang/en/docs/install/#debian-stable) for this.

Install project dependencies with command:

```bash
yarn install
```

That's all

### Launch dev server

Use command:

```bash
yarn dev
```

And after that in **SEPARATE** terminal:

```bash
yarn tauri dev
```

After that the development server should be ready to view.

## Build app

**AFTER** installing all dependencies, use command:

```bash
yarn build && yarn tauri build
```
